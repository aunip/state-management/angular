import { PizzaService } from '../pizza.service';

describe('Pizza Service', () => {
  let service: PizzaService;

  beforeEach(() => {
    service = new PizzaService();
  });

  it('readAllPizzas', async () => {
    const pizzas = await service.readAllPizzas();

    expect(pizzas).toHaveLength(25);
  });
});
