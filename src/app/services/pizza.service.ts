import { Injectable } from '@angular/core';
import Pizza from '../store/pizza.model';
import { generateId } from '../utils';
import pizzas from '../../pizzas.json';

@Injectable({
  providedIn: 'root'
})
export class PizzaService {
  readAllPizzas() {
    return new Promise(resolve => {
      resolve(
        pizzas.map((pizza: Pizza) => {
          pizza.id = generateId();
          return pizza;
        })
      );
    });
  }
}
