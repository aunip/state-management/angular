import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { PizzaService } from '../services/pizza.service';
import { SetPizzas } from './../store/pizza.actions';
import Pizza from '../store/pizza.model';

@Component({
  selector: 'app',
  template: `
    <router-outlet></router-outlet>
  `
})
export class AppComponent implements OnInit {
  constructor(private pizzaService: PizzaService, private store: Store<{ pizzas: Pizza[] }>) {}

  ngOnInit() {
    this.pizzaService.readAllPizzas().then((result: Pizza[]) => {
      this.store.dispatch(SetPizzas({ payload: result }));
    });
  }
}
