import { Component, Input } from '@angular/core';

@Component({
  selector: 'block',
  template: `
    <div class="block" [style.height]="height + 'px'">
      <div class="frame"></div>
      <div class="content" [style.background]="color">
        <ng-content></ng-content>
      </div>
    </div>
  `
})
export class BlockComponent {
  @Input() height = 60;
  @Input() color = '#ff7043';
}
