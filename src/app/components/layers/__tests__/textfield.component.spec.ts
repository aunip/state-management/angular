import { Component } from '@angular/core';
import { render } from '@testing-library/angular';
import { TextFieldComponent } from './../textfield.component';

@Component({
  template: `
    <text-field>
      <span>Hello World</span>
    </text-field>
  `
})
class HelloWorldComponent {}

describe('<text-field></text-field>', () => {
  it('Should Component Renders', async () => {
    const { container } = await render(TextFieldComponent);

    expect(container).toBeDefined();
  });

  it('Should Component Renders Simply', async () => {
    const { container, getByText } = await render(HelloWorldComponent, {
      declarations: [TextFieldComponent]
    });

    expect(container.querySelector('p').getAttribute('style')).toEqual('font-size: 16px;');

    expect(getByText('Hello World')).toBeInTheDocument();
  });

  it('Should Component Renders Fully', async () => {
    const { getByPlaceholderText } = await render(TextFieldComponent, {
      componentProperties: {
        type: 'number',
        placeholder: 'Test',
        value: '42',
        size: 18,
        editable: true
      }
    });

    expect(getByPlaceholderText('Test')).toHaveAttribute('type', 'number');
    expect(getByPlaceholderText('Test').getAttribute('style')).toEqual('font-size: 18px;');
  });

  it('Should Change Event Works Well', async () => {
    const handleInput = jest.fn();

    const { getByPlaceholderText, input } = await render(TextFieldComponent, {
      componentProperties: {
        placeholder: 'Test',
        handleInput: {
          emit: handleInput
        } as any,
        editable: true
      }
    });

    input(getByPlaceholderText('Test'), { target: { value: 'Hello World' } });

    expect(handleInput).toHaveBeenCalled();
  });
});
