import { Component } from '@angular/core';
import { render } from '@testing-library/angular';
import { HyperLinkComponent } from './../hyperlink.component';

@Component({
  template: `
    <hyper-link>Click Me</hyper-link>
  `
})
class ClickMeComponent {}

describe('<hyper-link></hyper-link>', () => {
  it('Should Component Renders', async () => {
    const { container } = await render(HyperLinkComponent);

    expect(container).toBeDefined();
  });

  it('Should Component Renders Simply', async () => {
    const { getByText } = await render(ClickMeComponent, {
      declarations: [HyperLinkComponent]
    });

    expect(getByText('Click Me')).toBeInTheDocument();
  });

  fit('Should Component Renders Fully', async () => {
    const { container } = await render(HyperLinkComponent, {
      componentProperties: { to: '#test', size: 18 }
    });

    const hyperlink = container.querySelector('a');

    expect(hyperlink).toHaveAttribute('href', '#test');

    expect(hyperlink.getAttribute('style')).toEqual('font-size: 18px;');
  });

  it('Should Click Event Works Well', async () => {
    const handleClick = jest.fn();

    const { container, click } = await render(HyperLinkComponent, {
      componentProperties: {
        handleClick: {
          emit: handleClick
        } as any
      }
    });

    click(container.querySelector('a'));

    expect(handleClick).toHaveBeenCalled();
  });
});
