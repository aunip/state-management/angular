import { render } from '@testing-library/angular';
import { RadioComponent } from './../radio.component';

describe('<radio></radio>', () => {
  it('Should Component Renders', async () => {
    const { container } = await render(RadioComponent);

    expect(container).toBeDefined();
  });

  it('Should Component Renders Fully', async () => {
    const { container } = await render(RadioComponent, {
      componentProperties: { color: '#fafafa' }
    });

    const icon = container.querySelector('svg');
    const circles = container.querySelectorAll('circle');

    expect(icon.getAttribute('style')).toEqual('cursor: pointer;');

    expect(circles).toHaveLength(2);
  });

  it('Should Click Event Works Well', async () => {
    const handleClick = jest.fn();

    const { container, click } = await render(RadioComponent, {
      componentProperties: {
        color: '#fafafa',
        handleClick: {
          emit: handleClick
        } as any
      }
    });

    const icon = container.querySelector('svg');

    // First Click
    click(icon);
    expect(container.querySelectorAll('circle')).toHaveLength(1);

    // Second Click
    click(icon);
    expect(container.querySelectorAll('circle')).toHaveLength(2);

    expect(handleClick).toHaveBeenCalledTimes(2);
  });
});
