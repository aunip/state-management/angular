import { Component } from '@angular/core';
import { render } from '@testing-library/angular';
import { RowComponent } from './../row.component';

@Component({
  template: `
    <row>
      <span leftCell>Left</span>
      <span rightCell>Right</span>
    </row>
  `
})
class CellComponent {}

describe('<row></row>', () => {
  it('Should Component Renders', async () => {
    const { container } = await render(RowComponent);

    expect(container).toBeDefined();
  });

  it('Should Component Renders Simply', async () => {
    const { container } = await render(RowComponent);

    expect(container.querySelector('.icon')).toBeInTheDocument();
  });

  it('Should Component Renders Fully', async () => {
    const { getByText } = await render(CellComponent, {
      declarations: [RowComponent]
    });

    expect(getByText('Left')).toBeInTheDocument();
    expect(getByText('Right')).toBeInTheDocument();
  });
});
