import { Component } from '@angular/core';
import { render } from '@testing-library/angular';
import { BlockComponent } from '../block.component';

@Component({
  template: `
    <block>
      <span>Hello World</span>
    </block>
  `
})
class HelloWorldComponent {}

describe('<block></block>', () => {
  it('Should Component Renders', async () => {
    const { container } = await render(BlockComponent);

    expect(container).toBeDefined();
  });

  it('Should Component Renders Simply', async () => {
    const { getByText } = await render(HelloWorldComponent, {
      declarations: [BlockComponent]
    });

    expect(getByText('Hello World')).toBeInTheDocument();
  });

  it('Should Component Renders Fully', async () => {
    const { container } = await render(BlockComponent, {
      componentProperties: { height: 100, color: '#212121' }
    });

    const block = container.querySelector('.block');
    const content = container.querySelector('.content');

    expect(block.getAttribute('style')).toEqual('height: 100px;');
    expect(content.getAttribute('style')).toEqual('background: rgb(33, 33, 33);');

    expect(container.querySelector('.frame')).toBeInTheDocument();
  });
});
