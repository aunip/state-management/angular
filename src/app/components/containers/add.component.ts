import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import Pizza from '../../store/pizza.model';
import { capitalize } from '../../utils';
import { AddPizza } from '../../store/pizza.actions';

@Component({
  selector: 'add',
  templateUrl: './add.component.html'
})
export class AddComponent {
  label = '';
  items = [''];
  price = 0;

  constructor(private router: Router, private store: Store<{ pizzas: Pizza[] }>) {}

  setLabel(event) {
    this.label = event.target.value;
  }

  setPrice(event) {
    this.price = parseFloat(event.target.value);
  }

  isValid(): boolean {
    const allItems: string[] = this.items.filter(item => item.length > 0);

    return this.label.length > 0 && allItems.length > 0 && this.price > 0;
  }

  addItem(index: number) {
    return event => {
      const value = event.target.value;

      const newItems: string[] = this.items.map((item, idx) => (idx === index ? value : item));

      if (value.length > 0) {
        if (this.items[index + 1] === undefined) {
          this.items = [...newItems, ''];
        } else {
          this.items = newItems;
        }
      } else {
        this.items = newItems.filter((_, idx) => idx !== index + 1);
      }
    };
  }

  navigateToHome() {
    this.router.navigateByUrl('/');
  }

  addNewPizza() {
    if (this.isValid()) {
      this.store.dispatch(
        AddPizza({
          payload: {
            label: capitalize(this.label),
            items: this.items.filter(item => item.length > 0),
            price: this.price
          }
        })
      );

      this.navigateToHome();
    }
  }

  trackByFunc(index: number): number {
    return index;
  }
}
