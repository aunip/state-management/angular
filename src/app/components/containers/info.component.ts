import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { DelPizza, SetPizza } from '../../store/pizza.actions';
import Pizza from '../../store/pizza.model';
import { selectPizzaById, selectPizzas } from '../../store/pizza.selectors';
import { capitalize } from '../../utils';

@Component({
  selector: 'info',
  templateUrl: './info.component.html'
})
export class InfoComponent implements OnInit, OnDestroy {
  id: string;
  label = '';
  items = [''];
  price = 0;
  takeOff: string[] = [];
  locked = true;
  pizzaSubscription: Subscription;

  constructor(private router: Router, private route: ActivatedRoute, private store: Store<{ pizzas: Pizza[] }>) {}

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');

    const pizza$ = this.store.pipe(select(state => selectPizzaById({ pizzas: selectPizzas(state) }, this.id)));

    this.pizzaSubscription = pizza$
      .pipe(
        map(p => {
          this.label = p.label;
          this.items = p.items;
          this.price = p.price;
        })
      )
      .subscribe();
  }

  setLabel(event) {
    this.label = event.target.value;
  }

  setItem(index: number) {
    return event => {
      this.items[index] = event.target.value;
    };
  }

  setPrice(event) {
    this.price = parseFloat(event.target.value);
  }

  handleTakeOff(item: string) {
    if (this.takeOff.find(i => i === item)) {
      this.takeOff = this.takeOff.filter(i => i !== item);
    } else {
      this.takeOff = [...this.takeOff, item];
    }
  }

  navigateToHome() {
    this.router.navigateByUrl('/');
  }

  removePizza() {
    this.store.dispatch(DelPizza({ payload: this.id }));
    this.navigateToHome();
  }

  updatePizza() {
    if (!this.locked) {
      this.store.dispatch(
        SetPizza({
          payload: {
            id: this.id,
            label: capitalize(this.label),
            items: this.items,
            price: this.price
          }
        })
      );
    }

    this.locked = !this.locked;
  }

  trackByFunc(index: number): number {
    return index;
  }

  ngOnDestroy() {
    this.pizzaSubscription.unsubscribe();
  }
}
