import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import Pizza from '../../store/pizza.model';
import { selectPizzas } from '../../store/pizza.selectors';
import { lo, scrollToListView } from '../../utils';

@Component({
  selector: 'list',
  templateUrl: './list.component.html'
})
export class ListComponent implements OnInit, OnDestroy {
  filter = '';
  pizzaSubscription: Subscription;
  pizza$: Observable<Pizza[]>;
  pizzas: Pizza[] = [];

  constructor(private router: Router, private store: Store<{ pizzas: Pizza[] }>) {
    this.pizza$ = store.pipe(select(selectPizzas));
  }

  ngOnInit() {
    setTimeout(() => {
      scrollToListView();
    }, 1000);

    this.pizzaSubscription = this.pizza$.pipe(map(p => (this.pizzas = p))).subscribe();
  }

  getAllPizzas(): Pizza[] {
    return this.pizzas;
  }

  getFilteredPizzas(): Pizza[] {
    return this.pizzas.filter(({ label }) => lo(label).includes(lo(this.filter)));
  }

  byKey(key: string) {
    return (a, b) => (a[key] > b[key] ? 1 : a[key] < b[key] ? -1 : 0);
  }

  navigateToNew() {
    this.router.navigateByUrl('/new');
  }

  navigateToPizza(id: string) {
    this.router.navigateByUrl(`/pizza/${id}`);
  }

  setFilter(event) {
    this.filter = event.target.value;
  }

  ngOnDestroy() {
    this.pizzaSubscription.unsubscribe();
  }
}
