import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent, InfoComponent, AddComponent } from './components/containers';

const routes: Routes = [
  { path: '', component: ListComponent },
  { path: 'pizza/:id', component: InfoComponent },
  { path: 'new', component: AddComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RoutingModule {}
