import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { RoutingModule } from './routing.module';
import { AppComponent } from './components/app.component';
import { ListComponent, InfoComponent, AddComponent } from './components/containers';
import { BlockComponent, HyperLinkComponent, RadioComponent, RowComponent, TextFieldComponent } from './components/layers';
import { PizzaService } from './services/pizza.service';
import { PizzaReducer } from './store/pizza.reducer';
import { environment } from '../environments/environment.prod';

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    AddComponent,
    InfoComponent,
    BlockComponent,
    HyperLinkComponent,
    RadioComponent,
    RowComponent,
    TextFieldComponent
  ],
  imports: [
    BrowserModule,
    RoutingModule,
    StoreModule.forRoot({ pizzas: PizzaReducer }),
    StoreDevtoolsModule.instrument({ logOnly: environment.production })
  ],
  providers: [PizzaService],
  bootstrap: [AppComponent]
})
export class AppModule {}
