import { Action } from '@ngrx/store';
import * as Actions from '../pizza.actions';
import { PizzaReducer as reducer } from '../pizza.reducer';
import * as Selectors from '../pizza.selectors';

describe('Pizza', () => {
  describe('Actions', () => {
    it('Should "SetPizzas" Action Returns Right "type" & "payload"', () => {
      const payload = [
        {
          id: 'uguj2keowpq',
          label: '4 Fromages',
          items: ['Mozzarella', 'Chèvre', 'Reblochon', 'Gorgonzola'],
          price: 14.9
        }
      ];

      expect(Actions.SetPizzas({ payload })).toEqual({
        type: 'PIZZA/SET_PIZZAS',
        payload
      });
    });

    it('Should "ResetPizzas" Action Returns Right "type" & "payload"', () => {
      expect(Actions.ResetPizzas()).toEqual({
        type: 'PIZZA/RESET_PIZZAS'
      });
    });

    it('Should "AddPizza" Action Returns Right "type" & "payload"', () => {
      const payload = {
        label: '4 Fromages',
        items: ['Mozzarella', 'Chèvre', 'Reblochon', 'Gorgonzola'],
        price: 14.9
      };

      expect(Actions.AddPizza({ payload })).toEqual({
        type: 'PIZZA/ADD_PIZZA',
        payload
      });
    });

    it('Should "SetPizza" Action Returns Right "type" & "payload"', () => {
      const payload = {
        id: 'uguj2keowpq',
        label: '4 Fromages',
        items: ['Mozzarella', 'Chèvre', 'Reblochon', 'Gorgonzola'],
        price: 14.9
      };

      expect(Actions.SetPizza({ payload })).toEqual({
        type: 'PIZZA/SET_PIZZA',
        payload
      });
    });

    it('Should "DelPizza" Action Returns Right "type" & "payload"', () => {
      expect(Actions.DelPizza({ payload: 'uguj2keowpq' })).toEqual({
        type: 'PIZZA/DEL_PIZZA',
        payload: 'uguj2keowpq'
      });
    });
  });

  describe('Reducer', () => {
    const initialState = [];

    it('Should Default Case Returns Right State', () => {
      expect(reducer(undefined, { type: 'DEFAULT' } as Action)).toEqual(initialState);
    });

    it('Should "PIZZA/SET_PIZZAS" Case Returns Right State', () => {
      const payload = [
        {
          id: 'uguj2keowpq',
          label: '4 Fromages',
          items: ['Mozzarella', 'Chèvre', 'Reblochon', 'Gorgonzola'],
          price: 14.9
        }
      ];

      expect(reducer(undefined, Actions.SetPizzas({ payload }))).toEqual(payload);
    });

    it('Should "PIZZA/RESET_PIZZAS" Case Returns Right State', () => {
      const state = [
        {
          id: 'uguj2keowpq',
          label: '4 Fromages',
          items: ['Mozzarella', 'Chèvre', 'Reblochon', 'Gorgonzola'],
          price: 14.9
        }
      ];

      expect(reducer(state, Actions.ResetPizzas())).toEqual(initialState);
    });

    it('Should "PIZZA/ADD_PIZZA" Case Returns Right State', () => {
      const payload = {
        label: '4 Fromages',
        items: ['Mozzarella', 'Chèvre', 'Reblochon', 'Gorgonzola'],
        price: 14.9
      };

      expect(reducer(undefined, Actions.AddPizza({ payload }))).toHaveLength(initialState.length + 1);
    });

    it('Should "PIZZA/SET_PIZZA" Case Returns Right State', () => {
      const state = [
        {
          id: 'uguj2keowpq',
          label: '4 Fromages',
          items: ['Mozzarella', 'Chèvre', 'Reblochon', 'Gorgonzola'],
          price: 14.9
        },
        {
          id: 'u91diii1em',
          label: 'Calzone',
          items: ['Mozzarella', 'Jambon Blanc', 'Champignons', 'Emmental', 'Oeuf', 'Origan'],
          price: 13.9
        }
      ];

      const payload = {
        id: 'uguj2keowpq',
        label: '3 Fromages',
        items: ['Mozzarella', 'Gouda', 'Reblochon', 'Gorgonzola'],
        price: 13.9
      };

      expect(reducer(state, Actions.SetPizza({ payload }))).toEqual(state.map(value => (value.id === payload.id ? payload : value)));
    });

    it('Should "PIZZA/DEL_PIZZA" Case Returns Right State', () => {
      const state = [
        {
          id: 'uguj2keowpq',
          label: '4 Fromages',
          items: ['Mozzarella', 'Chèvre', 'Reblochon', 'Gorgonzola'],
          price: 14.9
        }
      ];

      expect(reducer(state, Actions.DelPizza({ payload: 'uguj2keowpq' }))).toEqual(initialState);
    });
  });

  describe('Selectors', () => {
    const state = {
      pizzas: [
        {
          id: 'uguj2keowpq',
          label: '4 Fromages',
          items: ['Mozzarella', 'Chèvre', 'Reblochon', 'Gorgonzola'],
          price: 14.9
        }
      ]
    };

    it('selectPizzas', () => {
      expect(Selectors.selectPizzas({ pizzas: [] })).toHaveLength(0);
      expect(Selectors.selectPizzas(state)).toHaveLength(1);
    });

    it('selectPizzaById', () => {
      expect(Selectors.selectPizzaById({ pizzas: [] }, '')).toBeNull();
      expect(Selectors.selectPizzaById(state, 'uguj2keowpq')).toEqual({
        id: 'uguj2keowpq',
        label: '4 Fromages',
        items: ['Mozzarella', 'Chèvre', 'Reblochon', 'Gorgonzola'],
        price: 14.9
      });
    });
  });
});
