export default interface Pizza {
  id?: string;
  label: string;
  items: string[];
  price: number;
}
