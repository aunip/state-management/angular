import { createReducer, on, Action } from '@ngrx/store';
import * as PizzaActions from './pizza.actions';
import Pizza from './pizza.model';
import { generateId } from '../utils';

export const initialState = [];

const reducer = createReducer(
  initialState,
  on(PizzaActions.SetPizzas, (_: Pizza[], { payload }: { payload: Pizza[] }) => {
    return payload;
  }),
  on(PizzaActions.ResetPizzas, (_: Pizza[]) => initialState),
  on(PizzaActions.AddPizza, (state: Pizza[], { payload }: { payload: Pizza }) => {
    return [
      ...state,
      {
        id: generateId(),
        ...payload
      }
    ];
  }),
  on(PizzaActions.SetPizza, (state: Pizza[], { payload }: { payload: Pizza }) => {
    return state.map((pizza: Pizza) => {
      return pizza.id === payload.id ? payload : pizza;
    });
  }),
  on(PizzaActions.DelPizza, (state: Pizza[], { payload }: { payload: string }) => {
    return state.filter(({ id }: Pizza) => {
      return id !== payload;
    });
  })
);

type PizzasOrUndefined = Pizza[] | undefined;

export function PizzaReducer(state: PizzasOrUndefined, action: Action) {
  return reducer(state, action);
}
