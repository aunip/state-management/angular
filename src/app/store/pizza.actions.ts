import { createAction, props } from '@ngrx/store';
import Pizza from './pizza.model';

enum Types {
  SetPizzas = 'PIZZA/SET_PIZZAS',
  ResetPizzas = 'PIZZA/RESET_PIZZAS',
  AddPizza = 'PIZZA/ADD_PIZZA',
  SetPizza = 'PIZZA/SET_PIZZA',
  DelPizza = 'PIZZA/DEL_PIZZA'
}

export const SetPizzas = createAction(Types.SetPizzas, props<{ payload: Pizza[] }>());

export const ResetPizzas = createAction(Types.ResetPizzas);

export const AddPizza = createAction(Types.AddPizza, props<{ payload: Pizza }>());

export const SetPizza = createAction(Types.SetPizza, props<{ payload: Pizza }>());

export const DelPizza = createAction(Types.DelPizza, props<{ payload: string }>());
