# All U Need Is Pizza

> _Made With **NodeJS** 12_

Isomorphic Projects For Testing Technologies With CRUD Pattern

## File Structure

```
.
+-- src
  +-- app
    +-- components
      +-- containers
        +-- add.component.html
        +-- add.component.ts
        +-- index.ts
        +-- info.component.html
        +-- info.component.ts
        +-- list.component.html
        +-- list.component.ts
      +-- layers
        +-- __tests__
          +-- block.component.spec.ts
          +-- hyperlink.component.spec.ts
          +-- radio.component.spec.ts
          +-- row.component.spec.ts
          +-- textfield.component.spec.ts
        +-- block.component.ts
        +-- hyperlink.component.ts
        +-- index.ts
        +-- radio.component.ts
        +-- row.component.ts
        +-- textfield.component.ts
      +-- app.component.ts
    +-- services
      +-- __tests__
        +-- pizza.service.spec.ts
      +-- pizza.service.ts
    +-- store
      +-- __tests__
        +-- pizza.spec.ts
      +-- pizza.actions.ts
      +-- pizza.model.ts
      +-- pizza.reducer.ts
      +-- pizza.selectors.ts
    +-- utils
      +-- index.ts
    +-- app.module.ts
    +-- routing.module.ts
  +-- assets
    +-- fonts
      +-- Nunito-Bold.ttf
      +-- Nunito-Regular.ttf
  +-- environnements
    +-- environnement.prod.ts
    +-- environnement.ts
  +-- favicon.png
  +-- index.html
  +-- main.ts
  +-- pizzas.json
  +-- polyfills.ts
  +-- styles.scss
  +-- setupTests.ts
+-- .editorconfig
+-- .gitignore
+-- .prettierignore
+-- .prettierc
+-- angular.json
+-- browserslist
+-- jest.config.js
+-- LICENSE
+-- package.json
+-- README.md
+-- tsconfig.app.json
+-- tsconfig.json
+-- tsconfig.spec.json
+-- tslint.json
+-- yarn.lock
```

## Process

Repository:

```
git clone https://gitlab.com/aunip/state-management/augular.git
```

Install:

```
npm install
```

Launch:

```
npm run start
```

Test:

```
npm run test
```

Build:

```
npm run build
```

## License

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
